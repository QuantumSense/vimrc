set nocompatible
"""""""""""""""""""""""""""""""""""""""""""
"Begin Keymapping Section
"""""""""""""""""""""""""""""""""""""""""""
"Set the <leader> key to , rather than \
let mapleader = ","

"Quality of life changes WRT editing and sourcing the vimrc
nnoremap <leader>ev :e $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

"Do things 'right' and turn off the array keys noob
inoremap <UP> <NOP>
nnoremap <UP> <NOP>
inoremap <Down> <NOP>
nnoremap <Down> <NOP>
inoremap <Left> <NOP>
nnoremap <Left> <NOP>
inoremap <Right> <NOP>
nnoremap <Right> <NOP>

"Remap fold toggle
nnoremap <leader>f za

"Remap ; to : to save some keystrokes
nnoremap ; :

"indent whole file
noremap <F5> gg=G 

"switch splits round-robin
nnoremap <leader>ww <C-w>w
"switch to split above
nnoremap <leader>wj <C-w>j
"switch to split below
nnoremap <leader>wk <C-w>k
"switch to split left
nnoremap <leader>wh <C-w>h
"switch to split right
nnoremap <leader>wl <C-w>l

"switch tabs
nnoremap <leader><Tab> :tabnext<CR>
nnoremap <leader><S-Tab> :tabprev<CR>

"make j and k do what you'd expect (makes linewrap less annoying)
nmap j gj
nmap k gk

"fix backspace
set backspace=indent,eol,start

"switching buffers
nnoremap <leader>n :bnext<CR>
nnoremap <leader>p :bprev<CR>

"clear hilights from search
nnoremap - :nohls<CR>

"change to directory of file in current buffer
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

"""""""""""""""""""""""""""""""""""""""""""
"End Keymapping Section
"""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""

set showmatch  "show matching brackets

"Turn on syntax highlighting
syntax on "turn on syntax hilighting

"set colorscheme to something less evil
set t_Co=256
colorscheme desert

"highlight the current line
set cursorline

"""""""""""""""""""""""""""""""""""""""""""
"End colorization section
"""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""
"Begin Indentation Section
"""""""""""""""""""""""""""""""""""""""""""
"Turn on indenting based on file extension
filetype plugin indent on

"indentation options
set tabstop=3
set shiftwidth=3
set expandtab "expand tabs to spaces like a boss

"""""""""""""""""""""""""""""""""""""""""""
"End Indentation Section
"""""""""""""""""""""""""""""""""""""""""""

"Command tab completion
set wildmenu
set wildmode=list:longest,full

"Line numbering
set number

"""""""""""""""""""""""""""""""""""""""""""
"Begin search options
"""""""""""""""""""""""""""""""""""""""""""
"Turn on incremental search, and hilighting
set incsearch
set hlsearch
set ignorecase
set smartcase
"""""""""""""""""""""""""""""""""""""""""""
"End search options
"""""""""""""""""""""""""""""""""""""""""""
"Turn on code folding
set foldmethod=syntax
set foldnestmax=10
set foldlevel=5

"Backup options, turns it on, sets a specific temp and backup directory so that
"we don't litter files everywhere
set backup
set backupdir=~/.vim/bak
set directory=~/.vim/tmp

"set text width
set tw=110

"support all the line ending types
set fileformats=unix,dos,mac

set scrolloff=5 "keep at least 5 lines around the cursor

"turn off error bells
set noeb vb t_vb=

"turn off the mouse
set mouse=

"Turn off the ever-so-annoying auto line break
set textwidth=0 wrapmargin=0

"Turn *on* visual wrapping (won't insert an actual line break)
set wrap
